const csv = require('csvtojson')

function csvToJson(filePath) {
    return csv()
        .fromFile(filePath)
        .then((data) => {
            return data;

        })
}

module.exports = csvToJson;