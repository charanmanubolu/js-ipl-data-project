//Extra runs conceded per team in the year 2016
const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const deliveriesFile = path.join(__dirname, '..', 'data', 'deliveries.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '3-extraRunPerTeam.json')


function extraRunPerTeam() {
    csvToJson(matchesFile).then(matchesdata => {

        csvToJson(deliveriesFile).then(deliveriesdata => {
            let IDOf2016 = []
            try {
                for (let key of matchesdata) {
                    if (key.season === '2016') {
                        IDOf2016.push(key.id)
                    }

                }
            } catch (err) {
                console.log("error at matches data iteration :", err)
            }
            result = {}
            try {
                for (let element of deliveriesdata) {
                    if (IDOf2016.includes(element.match_id)) {
                        if (!result[element.bowling_team]) {
                            result[element.bowling_team] = parseInt(element.extra_runs)
                        } else {
                            result[element.bowling_team] += parseInt(element.extra_runs)
                        }
                    }
                }
            } catch (error) {
                console.log("Error at checking matches ids and deliveries match_id iteration :", error)
            }
            fs.writeFile(resultJsonFile, JSON.stringify(result), 'utf8', (err) => {
                if (err) {
                    console.log("Error at last :", err)
                } else {
                    console.log('problem completed and  data has been stored')
                }
            })
        })
    })
}
extraRunPerTeam()