//Number of matches played per year for all the years in IPL.
const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '1-matchesPerYear.json')

function matchesPerYear() {
    csvToJson(matchesFile).then(data => {
        let result = {}
        try {
            for (let elements of data) {
                if (elements.season in result) {
                    result[elements.season] += 1;
                } else {
                    result[elements.season] = 1;
                }
            }
        } catch (error) {
            console.log(error)
        }
        fs.writeFile(resultJsonFile, JSON.stringify(result), "utf8", (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(' Problem completed and result has been stored');
            }
        })
    })

}

matchesPerYear()