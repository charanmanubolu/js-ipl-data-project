//Find the highest number of times one player has been dismissed by another player

const fs = require('fs')
const csvtoJson = require('../data/utilities')
const path = require('path')
const matchesfile = path.join(__dirname, '..', 'data', 'matches.csv')
const deliveriesfile = path.join(__dirname, '..', 'data', 'deliveries.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '8-onePlayerDismissedByAnother')

function onePlayerDismissedByAnother() {
    csvtoJson(matchesfile).then(matchesdata => {
        csvtoJson(deliveriesfile).then(deliveriesdata => {
            let resultOfOnePlayerDismissesByAnother = {}
            try {
                for (let matches of matchesdata) {
                    for (let deliveries of deliveriesdata) {
                        if (matches.id === deliveries.match_id) {
                            if (deliveries.player_dismissed) {
                                let elementofBowlerAndplayer = [deliveries.bowler] + '$' + [deliveries.player_dismissed]
                                if (resultOfOnePlayerDismissesByAnother[elementofBowlerAndplayer]) {
                                    resultOfOnePlayerDismissesByAnother[elementofBowlerAndplayer] += 1

                                } else {
                                    resultOfOnePlayerDismissesByAnother[elementofBowlerAndplayer] = 1
                                }
                            }
                        }
                    }
                }
            } catch (err) {
                console.log(err)
            }
            let finallResult = []
            let max = 0
            try {
                for (let [key, values] of Object.entries(resultOfOnePlayerDismissesByAnother)) {
                    if (max < values) {
                        max = values
                    }
                    if (values == max) {
                        finallResult = key.split('$')

                    }
                }
            } catch (error) {
                console.log(error)
            }

            fs.writeFile(resultJsonFile, JSON.stringify(finallResult), 'utf8', (err => {
                if (err) {
                    console.log(err)
                } else {
                    console.log("problem sloved")
                }
            }))
        })
    })
}

onePlayerDismissedByAnother();