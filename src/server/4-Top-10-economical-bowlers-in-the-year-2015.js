//Top 10 economical bowlers in the year 2015
const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const deliveriesFile = path.join(__dirname, '..', 'data', 'deliveries.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '4-Top10EconomicalBowlersIn2015.json')

function top10EconomicalBowlers() {
    csvToJson(matchesFile).then(matchesdata => {

        csvToJson(deliveriesFile).then(deliveriesdata => {
            let IDOf2015 = []
            try {
                for (let key of matchesdata) {
                    if (key.season === '2015') {
                        IDOf2015.push(key.id)
                    }

                }
            } catch (err) {
                console.log("error at matches data iteration :", err)
            }
            let bowlerRunsAndBalls = {}
            try {
                for (let element of deliveriesdata) {
                    if (element.match_id in IDOf2015) {
                        if (bowlerRunsAndBalls[element.bowler]) {
                            if (bowlerRunsAndBalls[element.bowler]['runs']) {
                                bowlerRunsAndBalls[element.bowler]['runs'] += parseInt(element.total_runs)
                                bowlerRunsAndBalls[element.bowler]['balls'] += 1

                            } else {
                                bowlerRunsAndBalls[element.bowler]['runs'] = parseInt(element.total_runs)

                                bowlerRunsAndBalls[element.bowler]['balls'] += 1
                            }
                        } else {

                            bowlerRunsAndBalls[element.bowler] = { ['runs']: parseInt(element.total_runs), ['balls']: 1 }
                        }
                    }
                }
            } catch (error) {
                console.log(error)
            }

            let economy = {}
            for (let bowler in bowlerRunsAndBalls) {
                let ball = bowlerRunsAndBalls[bowler].balls
                let overs = ball / 6
                economy[bowler] = bowlerRunsAndBalls[bowler].runs / overs
            }

            let entriesOfEconomy = Object.entries(economy)
            let length = entriesOfEconomy.length

            for (let index = 0; index < length - 1; index++) {
                for (let j = 0; j < length - index - 1; j++) {
                    if (entriesOfEconomy[j][1] > entriesOfEconomy[j + 1][1]) {
                        [entriesOfEconomy[j], entriesOfEconomy[j + 1]] = [entriesOfEconomy[j + 1], entriesOfEconomy[j]]
                    }
                }
            }
            let Top10Economy = {}
            let count = 0
            for (let pairs of entriesOfEconomy) {
                if (count < 9) {
                    let [key, value] = pairs
                    Top10Economy[key] = value
                    count++;
                }
            }
            fs.writeFile(resultJsonFile, JSON.stringify(Top10Economy), 'utf8', ((err, data) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log("problem solved")
                }
            }))
        })
    })
}

top10EconomicalBowlers();