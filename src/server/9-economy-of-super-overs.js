//Find the bowler with the best economy in super overs

const fs = require("fs")
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const deliveriesFile = path.join(__dirname, '..', 'data', 'deliveries.csv')
const resultToJson = path.join(__dirname, '..', 'public', 'output', '9-bowlerWithSuperOverEconomy')

function superOverEconomy() {

    csvToJson(deliveriesFile).then(deliveriesdata => {
        let resultOFBowlers = {}
        try {
            for (let deliveries of deliveriesdata) {
                if (deliveries.is_super_over !== "0") {
                    if (resultOFBowlers[deliveries.bowler]) {
                        resultOFBowlers[deliveries.bowler]['runs'] += parseInt(deliveries.total_runs)
                        resultOFBowlers[deliveries.bowler]['balls'] += 1
                    } else {
                        resultOFBowlers[deliveries.bowler] = { ['runs']: parseInt(deliveries.total_runs), ['balls']: 1 }
                    }
                }
            }
        } catch (err) {
            console.log(err)
        }

        let resultOFeconomy = {}
        let max = null
        try {
            for (let [key, values] of Object.entries(resultOFBowlers)) {
                let overs = values.balls / 6
                let economy = values.runs / overs

                if (!max) {
                    max = economy
                }
                if (max > economy) {
                    max = economy
                    resultOFeconomy = key
                }
            }
        } catch (err) {
            console.log(err)
        }
        fs.writeFile(resultToJson, JSON.stringify(resultOFeconomy), 'utf8', (err => {
            if (err) {
                console.log(err)
            } else {
                console.log("problem solved")
            }
        }))

    })

}

superOverEconomy()