//find a player who has won the highest number of Player of the Match awards for each season

const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '6-playerOfTheMatch.json')

function playerOfTheMatch(params) {
    csvToJson(matchesFile).then(data => {
        let resultOfSeasonWithPlayer = {}
        try{
        for (let key of data) {
            if (resultOfSeasonWithPlayer[key.season]) {
                if (resultOfSeasonWithPlayer[key.season][key.player_of_match]) {
                    resultOfSeasonWithPlayer[key.season][key.player_of_match] += 1
                } else {
                    resultOfSeasonWithPlayer[key.season][key.player_of_match] = 1
                }

            } else {
                resultOfSeasonWithPlayer[key.season] = { [key.player_of_match]: 1 }
            }
        }
    }catch(error){
        console.log("your getting a error at when you finding the resultOfSeasonWithPlayer" , error)
    }
        let resultOfTopPlayerOfTheMatchForEachSeason = {}
        try{
        let entries = Object.entries(resultOfSeasonWithPlayer)
        for (let [season, playerdata] of entries) {
            let maxPerSeason = 0
            for (let [player, value] of Object.entries(playerdata)) {
                if (resultOfTopPlayerOfTheMatchForEachSeason[season]) {

                    if (maxPerSeason < value) {
                        maxPerSeason = value
                        resultOfTopPlayerOfTheMatchForEachSeason[season] = player
                    }
                } else {
                    resultOfTopPlayerOfTheMatchForEachSeason[season] = player
                }
            }
        }
    }catch(error){
        console.log("your getting error when you are finding the file result ", error)
    }
       fs.writeFile(resultJsonFile,JSON.stringify(resultOfTopPlayerOfTheMatchForEachSeason),'utf8',(err=>{
        if(err){
            console.log(err)
        }else{
            console.log("problem completed")
        }
       }))
    })
}

playerOfTheMatch()