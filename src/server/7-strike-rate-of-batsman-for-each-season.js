//Find the strike rate of a batsman for each season

const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const deliveriesFile = path.join(__dirname, '..', 'data', 'deliveries.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '7-strikeRateOfBatsmanForEachSeason.json')

function strikeRateOfBatsmanForEachSeason() {
    csvToJson(matchesFile).then(matchesdata => {
        csvToJson(deliveriesFile).then(deliveriesdata => {
            let resultOfSeasonAndBatsman = {}
            try{
            for (let matches of matchesdata) {
                for (let deliveries of deliveriesdata) {
                    if (matches.id === deliveries.match_id) {
                        if (resultOfSeasonAndBatsman[matches.season]) {
                            if (resultOfSeasonAndBatsman[matches.season][deliveries.batsman]) {
                                if (resultOfSeasonAndBatsman[matches.season][deliveries.batsman]['runs']) {
                                    resultOfSeasonAndBatsman[matches.season][deliveries.batsman]['runs'] += parseInt(deliveries.total_runs)
                                    resultOfSeasonAndBatsman[matches.season][deliveries.batsman]['balls'] += 1
                                } else {
                                    resultOfSeasonAndBatsman[matches.season][deliveries.batsman]['runs'] = parseInt(deliveries.total_runs)
                                    resultOfSeasonAndBatsman[matches.season][deliveries.batsman]['balls'] = 1
                                }
                            } else {
                                resultOfSeasonAndBatsman[matches.season] [deliveries.batsman]= { ['runs']: parseInt(deliveries.total_runs), ['balls']: 1 } 
                            }
                        } else {
                            resultOfSeasonAndBatsman[matches.season] = { [deliveries.batsman]: {} }

                        }
                    }

                }

            }
        }catch(error){
            console.log(error)
        }
            let strikeRateOFBatsman={}
            try{
            for(let season in resultOfSeasonAndBatsman){
               let playerRunsBalls=Object.entries(resultOfSeasonAndBatsman[season])
               for(let [key,value] of playerRunsBalls){
                let strike=value.runs/value.balls 
                if(strikeRateOFBatsman[season]){                
                    
                    strikeRateOFBatsman[season][key]=strike*100
                }else{
                    strikeRateOFBatsman[season]={[key]:strike*100}
                }
               }
            }
        }catch(error){
            console.log(error)
        }
            fs.writeFile(resultJsonFile,JSON.stringify(strikeRateOFBatsman),'utf-8',(err=>{
                if(err){
                    console.log(err)
                }else{
                    console.log("problem sloved")
                }
            }))

        })
    })

}

strikeRateOfBatsmanForEachSeason()