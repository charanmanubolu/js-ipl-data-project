//the number of times each team won the toss and and also match
const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchesFile = path.join(__dirname, '..', 'data', 'matches.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '5-teamsWonMatchAndtoss.json')


function teamsWonMatchAndToss() {
    csvToJson(matchesFile).then(matchesdata => {
        let result = {}
        try {
            for (let key of matchesdata) {
                if (key.toss_winner === key.winner) {
                    if (!result[key.toss_winner]) {
                        result[key.toss_winner] = 0
                    }
                    result[key.toss_winner] += 1
                }

            }
        } catch (error) {
            console.log(error)
        }
        fs.writeFile(resultJsonFile, JSON.stringify(result), 'utf8', (err, data) => {
            if (err) {
                console.log(err)
            } else {
                console.log("problem completed and data has been stored")
            }
        })
    })
}

teamsWonMatchAndToss()
