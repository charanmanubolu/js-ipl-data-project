const fs = require('fs')
const csvToJson = require('../data/utilities')
const path = require('path')
const matchescsvFile = path.join(__dirname, '..', 'data', 'matches.csv')
const resultJsonFile = path.join(__dirname, '..', 'public', 'output', '2-matchesWonPerYearPerTeam.json')

function matchesWonperYearPerTeam() {
    csvToJson(matchescsvFile).then(data => {

        let result = {}
        try {
            for (let keys of data) {
                if (result[keys.season]) {
                    if (result[keys.season][keys.winner]) {
                        result[keys.season][keys.winner] += 1
                    } else {
                        result[keys.season][keys.winner] = 1
                    }
                } else {
                    result[keys.season] = { [keys.winner]: 1 }
                }

            }
        } catch (error) {
            console.log(error)
        }
        fs.writeFile(resultJsonFile, JSON.stringify(result), 'utf8', (err) => {
            if (err) {
                console.log("ERROR", err)
            } else {
                console.log("problem sloved and result has been stored")
            }
        })
    })
}

matchesWonperYearPerTeam()